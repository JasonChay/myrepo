print("Enter a string:")
s = input()

def wc(s):
    wordcount = 0
    run = False
    if len(s) > 0:
        wordcount += 1
        run = True
    if s[len(s)-1] == ' ':
        wordcount -= 1
    if run:
        for i in range(len(s) - 1):
            if s[i] != ' ' and s[i+1] == ' ':
                wordcount += 1
    return wordcount

print(wc(s))
