def grid_sum(grid):
    total = 0
    for i in range(len(grid)):
        for j in range(len(grid[0])):
            total += grid[i][j]
    return total

grid = [[1,2,3],[4,5,6],[7,8,9]]
print(grid_sum(grid)) # returns 45

from copy import deepcopy

def grid_transpose(grid):
    transposed = deepcopy(grid)
    for i in range(len(grid)):
        for j in range(len(grid[0])):
            transposed[j][i] = grid[i][j]
    return transposed


print(grid_transpose(grid))
# print [[1,4,7],[2,5,8],[3,6,9]]
