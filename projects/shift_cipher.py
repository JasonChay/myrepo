from string import ascii_uppercase

def shift_encrypt(m, k):
    abc = ascii_uppercase
    cipher = []
    m = m.upper()
    for i in m:
        if not i.isupper():
            cipher.append(i)
        for j in range(len(abc)):
            if i == abc[j]:
                cipher.append(abc[(j+k) % 26])
    return ''.join(cipher)

def shift_decrypt(c, k):
    abc = ascii_uppercase
    message = []
    c = c.upper()
    for i in c:
        if not i.isupper():
            message.append(i)
        for j in range(len(abc)):
            if i == abc[j]:
                message.append(abc[(j-k) % 26])
    return ''.join(message)

print(shift_encrypt("The WORLD is cruel!", 1))
print(shift_decrypt("uif XPSME JT DSVFM!", 1))
