# Monoalphabetic cipher
import string
from collections import defaultdict

key = {
    'a': 'd',
    'b': 'a',
    'c': 'x',
    'd': 'e',
    'e': 'b',
    'f': 'f',
    'g': 'h',
    'h': 'g',
    'i': 'i',
    'j': 'k',
    'k': 'j',
    'l': 'm',
    'm': 'l',
    'n': 'o',
    'o': 'n',
    'p': 'q',
    'q': 'p',
    'r': 'r',
    's': 't',
    't': 's',
    'u': 'v',
    'v': 'w',
    'w': 'u',
    'x': 'c',
    'y': 'z',
    'z': 'y'
}

message = """Alice was beginning to get very tired of sitting by her sister on the
bank, and of having nothing to do. Once or twice she had peeped into the
book her sister was reading, but it had no pictures or conversations in
it, "and what is the use of a book," thought Alice, "without pictures or
conversations?"

So she was considering in her own mind (as well as she could, for the
day made her feel very sleepy and stupid), whether the pleasure of
making a daisy-chain would be worth the trouble of getting up and
picking the daisies, when suddenly a White Rabbit with pink eyes ran
close by her.

There was nothing so very remarkable in that, nor did Alice think it so
very much out of the way to hear the Rabbit say to itself, "Oh dear! Oh
dear! I shall be too late!" But when the Rabbit actually took a watch
out of its waistcoat-pocket and looked at it and then hurried on, Alice
started to her feet, for it flashed across her mind that she had never
before seen a rabbit with either a waistcoat-pocket, or a watch to take
out of it, and, burning with curiosity, she ran across the field after
it and was just in time to see it pop down a large rabbit-hole, under
the hedge. In another moment, down went Alice after it!"""

def monoalph_encrypt(message, key):
    encrypt = ""
    for i in message:
        if i.isupper():
            temp_char = i.lower()
            encrypt += key[temp_char].upper()
        elif i not in key:
            encrypt += i
        else:
            encrypt += key[i]
    return encrypt

# def monoalph_decrypt(cipher):
#     decrypt_key = {}
#     for k, v in key.items():
#         decrypt_key[v] = k
#     decrypt = ""
#     for i in cipher:
#         if i.isupper():
#             temp_char = i.lower()
#             decrypt += decrypt_key[temp_char].upper()
#         elif i not in key:
#             decrypt += i
#         elif i in key:
#             decrypt += i
#     return decrypt
#     pass

def frequency(cipher):
    frequency_dict = {
        'a': 0,
        'b': 0,
        'c': 0,
        'd': 0,
        'e': 0,
        'f': 0,
        'g': 0,
        'h': 0,
        'i': 0,
        'j': 0,
        'k': 0,
        'l': 0,
        'm': 0,
        'n': 0,
        'o': 0,
        'p': 0,
        'q': 0,
        'r': 0,
        's': 0,
        't': 0,
        'u': 0,
        'v': 0,
        'w': 0,
        'x': 0,
        'y': 0,
        'z': 0
    }
    frequency_dict = defaultdict(int)
    for i in cipher:
        if i.isupper():
            temp_char = i.lower()
            frequency_dict[temp_char] += 1
        elif i.isalpha():
            frequency_dict[i] += 1
    frequency_sort = sorted(frequency_dict, key=frequency_dict.get, reverse=True)
    for j in frequency_sort:
        print("{}: {}".format(j, frequency_dict[j]))

# ct = (monoalph_encrypt(message, key))
# print(monoalph_decrypt(ct, decrypt_key))
frequency(monoalph_encrypt(message, key))
