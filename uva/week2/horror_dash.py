test_cases = int(input())
for n in range(test_cases):
    case = [int(i) for i in input().split()]
    speed = max(case)
    print("Case " + str(n+1) + ": " +str(speed))
