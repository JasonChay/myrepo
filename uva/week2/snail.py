while True:
    H, U, D, F = [int(i) for i in input().split()]
    if H == 0:
        break
    F_val = F/100*U
    day = 0
    SH = 0
    while True:
        if U - F_val*day > 0:
            CF = U - F_val*day
        else:
            CF = 0
        SH += CF
        if SH > H:
            print("success on day " + str(day + 1))
            break
        SH -= D
        day += 1
        if SH < 0:
            print("failure on day " + str(day))
            break
