while True:
    n, p = [int(i) for i in input().split()]
    requirements = []
    proposals = []
    if n == 0 and p == 0:
        break
    for i in range(n):
        requirements.append(input())
    num_req = len(requirements)
    counter = 1
    for i in range(p):
        proposer = input()
        proposal = []
        pro_req = []
        c = input().split()
        d = float(c[0])
        r = int(c[1])
        for j in range(r):
            pro_req.append(input())
        pro_reqs = len(pro_req)
        proposal.append(proposer)
        proposal.append(d)
        proposal.append(pro_reqs)
        proposals.append(proposal)
    counter += 1
    candidates = []
    for i in range(len(proposals)):
        candidates.append(proposals[i][:1])
        RFP_index = candidates.index(min(candidates[i][1]))
        print("RFP #{}".format(counter))
