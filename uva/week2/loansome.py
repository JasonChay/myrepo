"""
def main():
    while True:
        i = input().split()
        duration = int(i[0])
        down = float(i[1])
        loan_cost = float(i[2])
        depreciation_records = int(i[3])
        car_cost = down + loan_cost # Check
        month = 0
        current_depreciation = 0.0
        dep_months = []
        new_depreciations = []
        if duration < 0 or duration > 100 or car_cost > 75000:
            return
        for _ in range(depreciation_records):
            records = input().split()
            dep_months.append(int(records[0]))
            new_depreciations.append(float(records[1]))
        dep_months.append(int(100000000))
        for n in range(len(dep_months)):
            if month == dep_months[n]:
                current_depreciation = new_depreciations[n]
                car_cost *= (1 - current_depreciation)
                loan_cost -= down
                if loan_cost < car_cost:
                    if month == 1:
                        print(str(month) + " month")
                        break
                    else:
                        print(str(month) + " months")
                        break
                month += 1
            else:
                while month != dep_months[n] and month <= duration:
                    car_cost *= (1 - current_depreciation)
                    loan_cost -= down
                    print(car_cost, loan_cost)
                    if loan_cost < car_cost:
                        if month == 1:
                            print(str(month) + " month")
                            break
                        else:
                            print(str(month) + " months")
                            break
                    month += 1


main()
"""
def debug(x, y, z, n):
    print(x)
    print(y)
    print(z)
    print(n)

def transaction():
    global month
    global car_cost
    global depreciation
    car_cost *= (1 - depreciation)
    global loan_cost
    global loan_pay
    loan_cost -= loan_pay
    debug(loan_cost, car_cost, depreciation, month)
    if loan_cost < car_cost:
        if month == 1:
            print(str(month) + " month")
            return True
        else:
            print(str(month) + " months")
            return True

def main():
    while True:
        i = input().split()
        duration = int(i[0])
        down = float(i[1])
        global loan_cost
        loan_cost = float(i[2])
        depreciation_records = int(i[3])
        global depreciation
        depreciation = 0
        global car_cost
        car_cost = down + loan_cost # Check
        global loan_pay
        loan_pay = loan_cost/duration
        global month
        month = 0
        if duration < 0 or duration > 100 or car_cost > 75000:
            return
        for i in range(depreciation_records):
            records = input().split()
            dep_month = int(records[0])
            if month == dep_month and not transaction:
                depreciation = float(records[1])
                transaction()
                month += 1

            elif i == depreciation_records - 1:
                while not transaction:
                    transaction()
                    month += 1

            else:
                while month != dep_month or not transaction:
                     transaction()
                     month += 1

main()
