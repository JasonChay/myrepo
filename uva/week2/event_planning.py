while True:
    valid = []
    try:
        participants, budget, hotels, weeks = [int(i) for i in input().split()]
    except:
        break
    for i in range(hotels):
        cost = int(input())
        beds = [int(n) for n in input().split()]
        for b in beds:
            if b >= participants and participants*cost <= budget:
                valid.append(b*cost)
    if len(valid) > 0:
        min_cost = min(valid)
        print(min_cost)
    else:
        print("stay home")
