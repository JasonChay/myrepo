run = True
while run == True:
    i = int(input())
    if i > 0:
        divisa = input()
        point_divisa = divisa.split()
        n = int(point_divisa[0])
        m = int(point_divisa[1])
        for _ in range(i):
            query = input()
            point_query = query.split()
            x = int(point_query[0])
            y = int(point_query[1])
            if x == n or y == m:
                print("divisa")
            elif x < n and y > m:
                print("NO")
            elif x > n and y > m:
                print("NE")
            elif x < n and y < m:
                print("SO")
            elif x > n and y < m:
                print("SE")
    else:
        run = False
