t = int(input())
for _ in range(t):
    line = input()
    arr = line.split()
    a = int(arr[0])
    b = int(arr[1])
    if a < b:
        print("<")
    elif a > b:
        print(">")
    elif a == b:
        print("=")
