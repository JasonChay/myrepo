def squares(n):
    """
    squares = []
    for i in range(n):
        i.append(i*i)
    return squares
    """
    # list comprehension
    # for every i in list of range(n), return i*i
    return [i*i for i in list(range(n))]

def first_letter(str_arr):
    return [s[0] for s in str_arr]

def add_five(n_arr):
    return [n+5 for n in n_arr]

def convert_to_int(str_arr):
    return [int(i) for i in str_arr]

input = "20 50"
y = [int(i) for i in input.split()]

# Print out:
# Case #1: 1
# Case #2: 2
# ...
# until 100

# print("Case #" + str(i) + "i " + str(i))
print ("Case #{}: {}", format(i,i))
