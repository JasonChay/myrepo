from random import randint

answer = randint(1, 100)

while True:
    print("Guess an integer between 1 and 100")
    try:
        guess = int(input())
    except ValueError:
        print("Please input an integer")
        continue
    if guess < answer:
        print("You are too low")
    elif guess > answer:
        print("You are too high")
    else:
        print("Congratulations! You got it")
        break
