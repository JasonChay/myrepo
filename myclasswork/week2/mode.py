"""
def mode(arr):
    arr.sort()
    val = arr[0]
    counter = 0
    best_val = arr[0]
    best_counter = 0
    for n in arr:
        if n == val:
            counter += 1
        else:
            if counter > best_counter:
                best_val = val
                best_counter = counter
            val = n
            counter = 1
    return best_val
"""

def mode(arr):
    freq = {}
    for elt in arr:
        if elt in freq:
            freq[elt] += 1
        else freq[elt] = 1
    return freq
    candidate_mode = None
    top_freq = 0
    for key, val in freq.items():
        if val > top_freq:
            candidate_mode = key
            top_freq = val
    return candidate_mode

arr = [2, 3, 5, 2, 5, 6, 7, 5, 2, 6, 8, 9, 5, 3, 2, 4, 5]
print(mode(arr))

# Tuples are immutable lists
a = (1, 2)
# Unlike lists, we cannot replace elements within the tuple
# Dictionary keys must be immutable, cannot use lists as keys, but tuples are okay
