def sum_normal(n):
    arr = [i for i in range(n+1)]
    return sum(arr)
    ##  return int(n*(n+1)/2)
def sum_squares(n):
    arr = [i*i for i in range(n+1)]
    return sum(arr)
def sum_exp(n, e):
    arr = [i**e for i in range(n+1)]
    return sum(arr)
def sum_odd(n):
    arr = [i for i in range(1, 2*n+1, 2) if i % 2 != 0]
    return sum(arr)
def sum_even(n):
    arr = [i for i in range(2, 2*n+2, 2) if i % 2 == 0]
    return sum(arr)

print(sum_normal(5))
print(sum_squares(5))
print(sum_exp(5, 3))
print(sum_odd(5))
print(sum_even(5))
