# Classwork exercises

def factorial(n):
    total = 1
    for i in range(2, n + 1):
        total *= i
    return total

def average(arr):
    return sum(arr)/len(arr)

def myrange(arr):
    return max(arr) - min(arr)

def median(arr):
    arr.sort()
    if(len(arr) % 2 == 1):
        return arr[int((len(arr) - 1) / 2)]
    else:
        return ((arr[int(len(arr)/2)] + arr[int(len(arr)/2 - 1)])/2)

for i in range(5):
    print(factorial(i))

arr = [3, 7, 2, 1, 4, 6, 5, 8]
print(average(arr))
print(myrange(arr))
print(median(arr))

def mycount(s, char):
    counter = 0
    for c in s:
        if(c == char):
            counter += 1
    return counter

def toupper(s):
    upper_s = ""
    for c in s:
        upper_s += c.captialize
        return upper_s
        
print(mycount("alakazam", 'a'))
