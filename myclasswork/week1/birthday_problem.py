# Birthday Problem

# Convention: 365 days in a year - means 365 birthdays
# January 1 -> 0
# December 31 -> 364
from random import randint

def exists_same(arr):
    arr.sort()
    i = 0
    while i < len(arr) - 1:
        if arr[i] == arr[i+1]:
            return True
        i += 1
    return False

def exists_same2(arr):
    for i in range(365):
        if arr.count(i) > 1:
            return True
    return False

num_simulations = 50000
num_exists_same = 0

for _ in range(num_simulations):
    bday_list = []
    for _ in range(23):
        bday_list.append(randint(0,364))
    if exists_same(bday_list):
        num_exists_same += 1

print(num_exists_same/num_simulations)
